#! /usr/bin/env/python311

from cv2 import imread, cvtColor, resize, threshold, adaptiveThreshold, imshow, waitKey, COLOR_BGR2GRAY, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY

if __name__ =='__main__':
    try:
        img = imread("picture.jpg")
        img = cvtColor(img, COLOR_BGR2GRAY)
        img = resize(img, (560, 900))

        _, result = threshold(img, 25, 200, THRESH_BINARY)

        adaptive_result = adaptiveThreshold(
            img, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 41, 5)

        imshow("result", result)
        imshow("original", img)
        waitKey(0)
    except AssertionError as msg:
        print(msg)
